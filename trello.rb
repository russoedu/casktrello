cask :v1 => 'trello' do
  version '1.0'
  sha256 '9dcf1cbe7dfac72d4fa73e441e7c152888c840edf50179ff36f6325d9e493f4a'

  url 'http://cl.ly/dtuB/Trello.dmg'
  name 'Trello'
  homepage 'http://trello.com'
  license :gratis

  app 'Trello.app'
end
